<?php
/*
Plugin Name: wpCampaignMonitor
Version: 0.1
Description: Simply send Wordpress transactional emails through Campaign Monitor smart emails.
Author: Blank Canvas Limited
Author URI: https://www.blankcanvas.hk/
Text Domain: wp-campaignmonitor
Domain Path: /languages
*/
/*
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation using version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
	Note: This piece of code has largely been inspired by the wpmandrill plugin (https://wordpress.org/plugins/wpmandrill/)
*/

wpCampaignMonitor::on_load();
class wpCampaignMonitor {
	static $conflict;
	static $connected;
	static $error;
	static $template;
	static $settings;
	static $php_wrapper;

	static function on_load() {
		self::$php_wrapper = dirname( __FILE__) . '/createsend-php-5.0.1';

		add_action('admin_init', array(__CLASS__, 'adminInit'));
		add_action('admin_menu', array(__CLASS__, 'adminMenu'));
		load_plugin_textdomain('wp-campaignmonitor', false, dirname( plugin_basename( __FILE__ ) ).'/languages');

		if( function_exists('wp_mail') ) {
			self::$conflict = true;
			add_action('admin_notices', array(__CLASS__, 'adminNotices'));
			return;
		}

		self::$conflict = false;
		self::$error = array();
		if( self::isConfigured() ) {
			function wp_mail( $to, $subject, $message, $headers = '', $attachments = array() ) {
				try {
					$sent = wpCampaignMonitor::mail($to, $subject, $message, $headers, $attachments);
					if ( is_wp_error($sent) || $sent->response[0]->Status != 'Accepted') {
						return wpCampaignMonitor::wp_mail_native( $to, $subject, $message, $headers, $attachments );
					}
					return true;
				}catch(Exception $e){
					return wpCampaignMonitor::wp_mail_native( $to, $subject, $message, $headers, $attachments ); 
				}
			}
		}
	}

	static function wp_mail_native( $to, $subject, $html, $headers = '', $attachments = array()) {
		require plugin_dir_path( __FILE__ ) . '/legacy/function.wp_mail.php';
	}
	
	static function mail( $to, $subject, $html, $headers = '', $attachments = array()) {
		try {
			self::getConnected();
			if ( !self::isConnected() ) throw new Exception('Invalid API Key');

			$message = apply_filters( 'wp_mail', compact( 'to', 'subject', 'message', 'headers', 'attachments' ) );

			require_once self::$php_wrapper.'/csrest_transactional_smartemail.php';
			$auth = array("api_key" => self::getAPIKey());
			$smart_email_id = self::getSmartemailID();
			$wrap = new CS_REST_Transactional_SmartEmail($smart_email_id, $auth);

			$message['headers'] = $headers;
			// Checking the user-specified headers
			if ( empty( $message['headers'] ) ) {
				$message['headers'] = array();
			} else {
				if ( !is_array( $message['headers'] ) ) {
					$tempheaders = explode( "\n", str_replace( "\r\n", "\n", $message['headers'] ) );
				} else {
					$tempheaders = $message['headers'];
				}
				$message['headers'] = array();
				// If it's actually got contents
				if ( !empty( $tempheaders ) ) {
					// Iterate through the raw headers
					foreach ( (array) $tempheaders as $header ) {
						if ( strpos($header, ':') === false ) continue;

						// Explode them out
						list( $name, $content ) = explode( ':', trim( $header ), 2 );

						// Cleanup crew
						$name    = trim( $name    );
						$content = trim( $content );

						switch ( strtolower( $name ) ) {
							case 'from':
								if ( strpos($content, '<' ) !== false ) {
									// So... making my life hard again?
									$from_name = substr( $content, 0, strpos( $content, '<' ) - 1 );
									$from_name = str_replace( '"', '', $from_name );
									$from_name = trim( $from_name );

									$from_email = substr( $content, strpos( $content, '<' ) + 1 );
									$from_email = str_replace( '>', '', $from_email );
									$from_email = trim( $from_email );
								} else {
									$from_name  = '';
									$from_email = trim( $content );
								}
								$message['from_email']  = $from_email;
								$message['from_name']   = $from_name;
								break;
							case 'cc':
								$cc = array_merge( (array) $cc, explode( ',', $content ) );
								$message['cc'];
								break;
							case 'bcc':
								$bcc = array_merge( (array) $bcc, explode( ',', $content ) );
								$message['bcc_address'] = $bcc;
								break;
							case 'reply-to':
								$message['headers'][trim( $name )] = trim( $content );
								break;
							case 'importance':
							case 'x-priority':
							case 'x-msmail-priority':
								if ( !$message['important'] ) $message['important'] = ( strpos(strtolower($content),'high') !== false ) ? true : false;
								break;
							default:
								if ( substr($name,0,2) == 'x-' ) {
									$message['headers'][trim( $name )] = trim( $content );
								}
								break;
						}
					}
				}
			}

			// Checking To: field
			if( !is_array($message['to']) ) $message['to'] = explode(',', $message['to']);

			// Letting user to filter/change the message payload
			$message['from_email']  = apply_filters('wp_mail_from', $message['from_email']);
			$message['from_name']   = apply_filters('wp_mail_from_name', $message['from_name']);

			$variables = self::getSmartemailVariables();
			$cpy = array();
			foreach ($variables as $key => $value) {
				if ($value == 'message') $cpy[$key] = $html;
				if ($value == 'subject') $cpy[$key] = $message['subject'];
				if ($value == 'fromName') $cpy[$key] = $message['from_name'];
				if ($value == 'fromEmail') $cpy[$key] = $message['from_email'];
			}

			$complex_message = array(
					"ReplyTo" => $message['headers']['reply-to'],
					"Subject" => $message['subject'],
					"Data" => $cpy,
					"To" => $message['to'],
					"CC" => $message['cc'],
					"BCC" => $message['bcc_address'],
					"Attachments" => $message['attachments']
			);
			$result = $wrap->send($complex_message);
			return $result;
		} catch ( Exception $e ) {
			return new WP_Error( $e->getMessage() );
		}
	}

	// set up option page
	static function adminInit() {
		add_filter('plugin_action_links',array(__CLASS__,'showPluginActionLinks'), 10, 5);

		register_setting('wp-campaignmonitor', 'wp-campaignmonitor', array(__CLASS__,'formValidate'));
		add_settings_section('wp-campaignmonitor-api', __('API Settings', 'wp-campaignmonitor'), '__return_false', 'wp-campaignmonitor');
		add_settings_field('api-key', __('API key', 'wp-campaignmonitor'), array(__CLASS__, 'askAPIKey'), 'wp-campaignmonitor', 'wp-campaignmonitor-api');

		self::getConnected();

		if (self::getOption('client_id') || !empty(self::getClients()))
			add_settings_field('client-id', __('Client', 'wp-campaignmonitor'), array(__CLASS__, 'askClientID'), 'wp-campaignmonitor', 'wp-campaignmonitor-api');

		if (self::getOption('smartemail_id') || !empty(self::getSmartemails()))
			add_settings_field('smartemail-id', __('SmartEmail', 'wp-campaignmonitor'), array(__CLASS__, 'askSmartemailID'), 'wp-campaignmonitor', 'wp-campaignmonitor-api');

		if (self::getOption('smartemail_var') || !empty(self::getSmartemailsDetails()))
			add_settings_field('smartemail-var', __('SmartEmail variables', 'wp-campaignmonitor'), array(__CLASS__, 'askSmartemailVar'), 'wp-campaignmonitor', 'wp-campaignmonitor-api');
	}

	// Creates option page's entry in Settings section of menu.
	static function adminMenu() {
		self::$settings = add_options_page(
				__('Campaign Monitor Settings', 'wp-campaignmonitor'),
				__('Campaign Monitor', 'wp-campaignmonitor'),
				'manage_options',
				'wp-campaignmonitor',
				array(__CLASS__,'showOptionsPage')
				);
	}

	// Generates source of options page.
	static function showOptionsPage() {
		if (!current_user_can('manage_options'))
			wp_die( __('You do not have sufficient permissions to access this page.') );
		?>
		<div class="wrap" id="campaignmonitor-form-wrapper">
			<h2><?php _e('Campaign Monitor Settings', 'wp-campaignmonitor'); ?></h2>

			<div style="float: left;width: 70%;">
				<form method="post" action="options.php" id="form-campaignmonitor">
					<div>
						<?php settings_fields('wp-campaignmonitor'); ?>
						<?php do_settings_sections('wp-campaignmonitor'); ?>
					</div>
					<p class="submit"><input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" /></p>
				</form>
			</div> 
		</div>
		<?php
	}

	static function askAPIKey() {
		echo '<div class="inside">';

		$api_key = self::getOption('api_key');
		?><input id='api_key' name='wp-campaignmonitor[api_key]' size='75' type='text' value="<?php esc_attr_e( $api_key ); ?>" /><?php

		$where_to_find_api_key = __('
				Login with an administrator account to <a target="_blank" href="https://www.campaignmonitor.com/">Campaign Monitor</a>,<br/>
				go to account settings and click on something called “Get you API key” (or similar).<br/>
				You will get an API key at the bottom of your page.<br/>
				This is NOT the api key you\'re going to use.<br/><br/>
				Then click at "clients" up to the left,<br/>
				click on your username,<br/>
				click on "client setting",<br/>
				click on "edit" on the side of your name,<br/>
				and click on "Show client\'s API Info"<br/>
				--> Here is the correct API info.', 'wp-campaignmonitor');

			if ( empty($api_key) ) {
			?><br/><span class="setting-description"><small><em><?php echo $where_to_find_api_key; ?></em></small></span><?php
			} else {
				if ( !self::isConnected() ) {
					?><br/><span class="setting-description"><small><em><?php _e('The API key seems to be incorrect.', 'wp-campaignmonitor'); ?><br/><?php echo $where_to_find_api_key; ?></em></small></span><?php
				}
			}
		echo '</div>';
	}

	static function askClientID() {
		$clientID = self::getOption('client_id'); 
	?>
		<div class="inside">
			<select id="client_id" name="wp-campaignmonitor[client_id]">
				<option value="" disabled selected></option>

				<?php foreach(self::getClients() as $client_id => $client_name) { ?>
					<option <?php echo ($clientID == $client_id? "selected":""); ?> value="<?php echo $client_id; ?>"><?php echo $client_name; ?></option> 
				<?php }	?>

			</select>
		<?php
		if ( !$clientID ) {
			?><br/><span class="setting-description"><small><em><?php _e('Please chose a client associated to this API key.', 'wp-campaignmonitor'); ?></em></small></span><?php
		}
		?></div><?php
	}

	static function askSmartemailID() {
		$smartemailID = self::getOption('smartemail_id'); 
	?>
		<div class="inside">
			<select id="smartemail_id" name="wp-campaignmonitor[smartemail_id]">
				<option value="" disabled selected></option>

				<?php foreach(self::getSmartemails() as $smartemail_id => $smartemail_name) { ?>
					<option <?php echo ($smartemailID == $smartemail_id? "selected":""); ?> value="<?php echo $smartemail_id; ?>"><?php echo $smartemail_name; ?></option>
				<?php } ?>

			</select>
			<?php
			if ( !$smartemailID ) {
				?><br /><span class="setting-description"><small><em><?php _e('Please chose a smartemail associated to this client.', 'wp-campaignmonitor'); ?></em></small></span><?php
			}
	?>
		</div>
	<?php
	}

	static function askSmartemailVar() {
		$saved_variables = self::getOption('smartemail_var'); 
		$smartemaildetails = self::getSmartemailsDetails();
		if (!empty($smartemaildetails)) {
	?>
		<div class="inside">
			<style>
				#table-email-variables tr td{
					padding: 0px;
				}
			</style>
			<a target="_blank" href="<?php echo $smartemaildetails['HtmlPreviewUrl'] ?>">HTML preview URL</a><br />
			<?php
			echo '<table id="table-email-variables"><tr><td>Email variable in template</td><td>Type of content</td></tr>';
			foreach ($smartemaildetails['EmailVariables'] as $variable) {
				echo '<tr><td>'. $variable . '</td>';
				echo '
					<td><select id="smartemail_var" name="wp-campaignmonitor[smartemail_var]['. $variable .']">
					<option value="" disabled selected></option>
					<option '. ($saved_variables[$variable] == 'message'? "selected":"") .' value="message">Content</option>
					<option '. ($saved_variables[$variable] == 'fromName'? "selected":"") .' value="fromName">From: Name</option>
					<option '. ($saved_variables[$variable] == 'fromEmail'? "selected":"") .' value="fromEmail">From: Email</option>
					<option '. ($saved_variables[$variable] == 'subject'? "selected":"") .' value="subject">Subject</option>
					</select></td></tr>
				';
			}
			echo '</table>';
			if ( !$saved_variables ) {
				?><span class="setting-description"><small><em><?php _e('Please setup the relationship between the variables  associated to this smartemail.', 'wp-campaignmonitor'); ?></em></small></span><?php
			}
			?>
		</div>
	<?php
		}
	}

	static function getClients() {
		require_once self::$php_wrapper.'/csrest_general.php';
		$auth = array("api_key" => self::getAPIKey());
		$wrap = new CS_REST_General($auth);
		$result = $wrap->get_clients();

		if (!isset ($result->response) || empty($result->response) || (isset($result->response) && $result->response->Code == 50)) return array();
		else $output = array();

		foreach($result->response as $client)
			$output[$client->ClientID] = $client->Name;

		return $output;
	}

	static function getSmartemails(){
		require_once self::$php_wrapper.'/csrest_transactional_smartemail.php';
		$auth = array("api_key" => self::getAPIKey());
		$wrap = new CS_REST_Transactional_SmartEmail(self::getClientID(), $auth);
		$result = $wrap->get_list();

		if (!isset ($result->response) || empty($result->response) || (isset($result->response) && $result->response->Code == 50)) return array();
		else $output = array();

		foreach($result->response as $smart_email) {
			// only take the active ones
			if($smart_email->Status == 'active') {
				$output[$smart_email->ID] = $smart_email->Name;
			}
		}
		return $output;
	}

	static function getSmartemailsDetails(){
		require_once self::$php_wrapper.'/csrest_transactional_smartemail.php';
        $auth = array("api_key" => self::getAPIKey());
		$smart_email_id = self::getSmartemailID();
		$wrap = new CS_REST_Transactional_SmartEmail($smart_email_id, $auth);
		$result = $wrap->get_details($smart_email_id);

		if (!isset ($result->response) || empty($result->response) || (isset($result->response) && $result->response->Code == 50)) return array();
		else $output = array();

		if (isset ($result->response->Properties->Content->EmailVariables) ) {
			$output['EmailVariables'] = array();
			foreach($result->response->Properties->Content->EmailVariables as $variable) {
				array_push($output['EmailVariables'], $variable);
			}
		}
		if ( isset($result->response->Properties->HtmlPreviewUrl))
			$output['HtmlPreviewUrl'] = $result->response->Properties->HtmlPreviewUrl;
		return $output;
	}

	// Return mixed
	static function getOption( $name, $default = false ) {
		$options = get_option('wp-campaignmonitor');
		if( isset( $options[$name] ) )
			return $options[$name];
		return $default;
	}

    // Return boolean
	static function setOption( $name, $value ) {
		$options = get_option('wp-campaignmonitor');
		$options[$name] = $value;
		$result = update_option('wp-campaignmonitor', $options);
		return $result;
	}

	static function formValidate($input) {
		if (isset($input['api_key']))
			$input['api_key'] = wp_strip_all_tags($input['api_key']);

		if (isset($input['smartemail_id']))
			$input['smartemail_id'] = wp_strip_all_tags($input['smartemail_id']);

		if (isset($input['client_id']))
			$input['client_id'] = wp_strip_all_tags($input['client_id']);

		if (isset($input['smartemail_var'])) {
			$cpy = $input['smartemail_var'];

			foreach($input['smartemail_var'] as $key => $value)
				$cpy[$key] = wp_strip_all_tags($value);

			$input['smartemail_var'] = $cpy;
		}

		$response = $input;
		return $response;
	}

	static function adminNotices() {
		if ( self::$conflict ) {
			echo '<div class="error"><p>'.__('Campaign Monitor: wp_mail has been declared by another process or plugin, so you won\'t be able to use Campaign Monitor until the problem is solved.', 'wp-campaignmonitor') . '</p></div>';
		}
	}

	// Return boolean
	static function isConnected() {
		return self::$connected;
	}

	static function getConnected() {
		if ( !self::$connected ) {
			try {
				require_once self::$php_wrapper.'/csrest_general.php';
				$auth = array("api_key" => self::getAPIKey());
				$wrap = new CS_REST_General($auth);
				$result = $wrap->get_systemdate();
				// if can get system date that means key is ok
				if( $result->was_successful() ) self::$connected = true;
				else {
					self::$connected = false;
					array_push(self::$error, 'API key incorrect.');
				}
			} catch ( Exception $e ) {}
		}
	}

	// Return boolean
	static function isConfigured() {
		return self::getAPIKey() && self::getClientID() && self::getSmartemailID() && self::getSmartemailVariables();
	}

	// Return string|boolean
	static function getAPIKey() {
		return self::getOption('api_key');
	}

	// Return string|boolean
	static function getClientID() {
		return self::getOption('client_id');
	}

	// Return string|boolean
	static function getSmartemailID() {
		return self::getOption('smartemail_id');
	}

	// Return array|boolean
	static function getSmartemailVariables() {
		return self::getOption('smartemail_var');
	}

	// Adds link to settings page in list of plugins
	static function showPluginActionLinks($actions, $plugin_file) {
		static $plugin;

		if (!isset($plugin))
			$plugin = plugin_basename(__FILE__);

		if ($plugin == $plugin_file) {
			$settings = array('settings' => '<a href="options-general.php?page=wp-campaignmonitor">' . __('Settings', 'wp-campaignmonitor') . '</a>');
			$actions = array_merge($settings, $actions);
		}

		return $actions;
	}
}
